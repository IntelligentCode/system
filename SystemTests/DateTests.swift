//
//  DateTests.swift
//  System
//
//  Created by Artem Salimyanov on 02.02.17.
//  Copyright © 2017 Артём Шляхтин. All rights reserved.
//

import XCTest
@testable import System

class DateTests: XCTestCase {
    
    var dateFormatter: DateFormatter {
        get {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            return dateFormatter
        }
    }
    
    var testedDate: Date {
        get {
            let date = dateFormatter.date(from: "2016-02-03 23:39:54")
            if let date = date {
                return date
            } else {
                XCTAssert(false, "Uncorrect determine date.")
                return Date()
            }
        }
    }
    
    /**
    Test day func
    */
    func testGettingDayFromDate() {
        let dayOfDate = testedDate.day()
        XCTAssertEqual(dayOfDate, 3)
    }
    
    /**
     Test month func
    */
    func testGettingMonthFromDate() {
        let monthOfDate = testedDate.month()
        XCTAssertEqual(monthOfDate, 2)
    }
    
    /**
     Test year func
    */
    func testGettingYearFromDate() {
        let yearOfDate = testedDate.year()
        XCTAssertEqual(yearOfDate, 2016)
    }
    
    /**
    Test hours func
    */
    func testGettingHourFromDate() {
        let hourOfDate = testedDate.hours()
        XCTAssertEqual(hourOfDate, 23)
    }
    
    /**
    Test minutes func
    */
    func testGettingMinuteFromDate() {
        let minutesOfDate = testedDate.minutes()
        XCTAssertEqual(minutesOfDate, 39)
    }
    
    /**
    Test dateAtBeginningOfDay func
    */
    func testGettingDayWithZeroTimePart() {
        let datewithZeroTimePart = testedDate.dateAtBeginningOfDay()
        print("\(dateFormatter.string(from: datewithZeroTimePart!))")
        if let date = datewithZeroTimePart {
            XCTAssertEqual([date.year(), date.month(), date.day(), date.hours(), date.minutes()],
                           [2016, 2, 3, 0, 0])
        } else {
            XCTAssert(false, "Uncorrect determine dateWithZeroTimePart")
        }
    }
    
    /**
    Test dateByAddingMinutes
    */
    func testAddedMinutesToCurrentDate() {
        let newDate = testedDate.dateByAddingMinutes(7)
        XCTAssertEqual(newDate.minutes(), 39+7)
        XCTAssertEqual([newDate.year(), newDate.month(), newDate.day(), newDate.hours()],
                       [2016, 02, 03, 23])
    }
    
    /**
     Test dateByAddingHours
     */
    func testAddedHoursToCurrentDate() {
        let newDate = testedDate.dateByAddingHours(7)
        XCTAssertEqual(newDate.hours(), 6 /* 23 + 7 */)
        XCTAssertEqual([newDate.year(), newDate.month(), newDate.day(), newDate.minutes()],
                       [2016, 02, 04, 39])
    }
    
    /**
     Test dateByAddingDays
     */
    func testAddedDaysToCurrentDate() {
        let newDate = testedDate.dateByAddingDays(7)
        XCTAssertEqual(newDate.day(), 10)
        XCTAssertEqual([newDate.year(), newDate.month(), newDate.hours(), newDate.minutes()],
                       [2016, 02, 23, 39])
    }
    
}
