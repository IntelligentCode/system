//
//  String+ClearFormat.swift
//  System
//
//  Created by Артём Шляхтин on 15.06.16.
//  Copyright © 2016 Артем Шляхтин. All rights reserved.
//

import UIKit

extension String {
    
    // MARK: - String Content
    
    /**
     Функция очищает строку формата +7 (985) 100-20-20 оставляя только цифры.
     
     - important: Во время очистки удаляет первую цифру.
     - returns: Возвращает строку содержащую только цифры.
    */
    public func clearPhoneFormat() -> String {
        var result = self.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
        result.remove(at: result.startIndex)
        return String(result)
    }
    
    /**
     Функция очищает строку содержащую номер банковской карты оставляя только цифры
     
     - returns: Возвращает строку содержащую только цифры.
    */
    public func clearCardFormat() -> String {
        let result = self.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
        return String(result)
    }

}

// MARK: - Card Format

extension String {
    
    public var formatCreditCardNumber: String? {
        get {
            if self.characters.count != 16 { return nil }
            
            let firstRange = self.characters.index(self.startIndex, offsetBy: 4)
            let secondRange = self.index(self.startIndex, offsetBy: 4)...self.index(self.startIndex, offsetBy: 7)
            let thirdRange = self.index(self.startIndex, offsetBy: 8)...self.index(self.startIndex, offsetBy: 11)
            let lastRange = self.characters.index(self.endIndex, offsetBy: -4)
            
            let formatCardNumber = String(format: "%@ %@ %@ %@", self.substring(to: firstRange),
                                          self[secondRange],
                                          self[thirdRange],
                                          self.substring(from: lastRange))
            return formatCardNumber
        }
    }
    
}
