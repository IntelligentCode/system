//
//  UIDevice+Jailbreak.swift
//  System
//
//  Created by Артём Шляхтин on 10/01/2017.
//  Copyright © 2017 Артем Шляхтин. All rights reserved.
//

import UIKit

// MARK: - Jailbreak

extension UIDevice {
    
    /**
     Свойство проверяет произведен ли на устройстве Jailbreak.
    */
    public var isJailbroken: Bool {
        get {
            guard let scheme = URL(string: "cydia://package/com.example.package") else {
                return isJailbrokenDevice()
            }
            return UIApplication.shared.canOpenURL(scheme) || isJailbrokenDevice()
        }
    }
    
    fileprivate func isJailbrokenDevice() -> Bool {
        
        if deviceType == .simulator {
            return false
        }
        
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: "/Applications/Cydia.app") ||
            fileManager.fileExists(atPath: "/Library/MobileSubstrate/MobileSubstrate.dylib") ||
            fileManager.fileExists(atPath: "/bin/bash") ||
            fileManager.fileExists(atPath: "/usr/sbin/sshd") ||
            fileManager.fileExists(atPath: "/etc/apt") ||
            fileManager.fileExists(atPath: "/usr/bin/ssh") {
            return true
        }
        
        if canOpen(path: "/Applications/Cydia.app") ||
            canOpen(path: "/Library/MobileSubstrate/MobileSubstrate.dylib") ||
            canOpen(path: "/bin/bash") ||
            canOpen(path: "/usr/sbin/sshd") ||
            canOpen(path: "/etc/apt") ||
            canOpen(path: "/usr/bin/ssh") {
            return true
        }
        
        let path = "/private/" + NSUUID().uuidString
        do {
            try "anyString".write(toFile: path, atomically: true, encoding: String.Encoding.utf8)
            try fileManager.removeItem(atPath: path)
            return true
        } catch {
            return false
        }
    }
    
    fileprivate func canOpen(path: String) -> Bool {
        let file = fopen(path, "r")
        guard file != nil else { return false }
        fclose(file)
        return true
    }
}

// MARK: - Model

extension UIDevice {
    
    public enum DeviceTypeIdiom : String {
        case simulator = "simulator/sandbox",
        iPod1          = "iPod 1",
        iPod2          = "iPod 2",
        iPod3          = "iPod 3",
        iPod4          = "iPod 4",
        iPod5          = "iPod 5",
        iPad2          = "iPad 2",
        iPad3          = "iPad 3",
        iPad4          = "iPad 4",
        iPhone4        = "iPhone 4",
        iPhone4S       = "iPhone 4S",
        iPhone5        = "iPhone 5",
        iPhone5S       = "iPhone 5S",
        iPhone5C       = "iPhone 5C",
        iPadMini1      = "iPad Mini 1",
        iPadMini2      = "iPad Mini 2",
        iPadMini3      = "iPad Mini 3",
        iPadAir1       = "iPad Air 1",
        iPadAir2       = "iPad Air 2",
        iPhone6        = "iPhone 6",
        iPhone6plus    = "iPhone 6 Plus",
        iPhone6S       = "iPhone 6S",
        iPhone6Splus   = "iPhone 6S Plus",
        iPhoneSE       = "iPhone SE",
        iPhone7        = "iPhone 7",
        iPhone7plus    = "iPhone 7 Plus",
        unrecognized   = "?unrecognized?"
    }
    
    var modelMap: [String: DeviceTypeIdiom] {
        get { let dict: [String: DeviceTypeIdiom] = [
             "i386"      : .simulator,
             "x86_64"    : .simulator,
             "iPod1,1"   : .iPod1,
             "iPod2,1"   : .iPod2,
             "iPod3,1"   : .iPod3,
             "iPod4,1"   : .iPod4,
             "iPod5,1"   : .iPod5,
             "iPad2,1"   : .iPad2,
             "iPad2,2"   : .iPad2,
             "iPad2,3"   : .iPad2,
             "iPad2,4"   : .iPad2,
             "iPad2,5"   : .iPadMini1,
             "iPad2,6"   : .iPadMini1,
             "iPad2,7"   : .iPadMini1,
             "iPhone3,1" : .iPhone4,
             "iPhone3,2" : .iPhone4,
             "iPhone3,3" : .iPhone4,
             "iPhone4,1" : .iPhone4S,
             "iPhone5,1" : .iPhone5,
             "iPhone5,2" : .iPhone5,
             "iPhone5,3" : .iPhone5C,
             "iPhone5,4" : .iPhone5C,
             "iPad3,1"   : .iPad3,
             "iPad3,2"   : .iPad3,
             "iPad3,3"   : .iPad3,
             "iPad3,4"   : .iPad4,
             "iPad3,5"   : .iPad4,
             "iPad3,6"   : .iPad4,
             "iPhone6,1" : .iPhone5S,
             "iPhone6,2" : .iPhone5S,
             "iPad4,1"   : .iPadAir1,
             "iPad4,2"   : .iPadAir2,
             "iPad4,4"   : .iPadMini2,
             "iPad4,5"   : .iPadMini2,
             "iPad4,6"   : .iPadMini2,
             "iPad4,7"   : .iPadMini3,
             "iPad4,8"   : .iPadMini3,
             "iPad4,9"   : .iPadMini3,
             "iPhone7,1" : .iPhone6plus,
             "iPhone7,2" : .iPhone6,
             "iPhone8,1" : .iPhone6S,
             "iPhone8,2" : .iPhone6Splus,
             "iPhone8,4" : .iPhoneSE,
             "iPhone9,1" : .iPhone7,
             "iPhone9,2" : .iPhone7plus,
             "iPhone9,3" : .iPhone7,
             "iPhone9,4" : .iPhone7plus
            ]
            return dict
        }
    }
    
    public var deviceType: DeviceTypeIdiom {
        var systemInfo = utsname()
        uname(&systemInfo)
        let modelCode = withUnsafePointer(to: &systemInfo.machine) {
            $0.withMemoryRebound(to: CChar.self, capacity: 1) {
                ptr in String.init(validatingUTF8: ptr)
            }
        }
        
        guard let code = modelCode,
              let model = modelMap[String.init(code)]
        else { return .unrecognized }
        
        return model
    }
    
}
