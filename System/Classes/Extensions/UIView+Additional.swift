//
//  UIView+Additional.swift
//  System
//
//  Created by Артём Шляхтин on 27/03/2017.
//  Copyright © 2017 Artem Shlyakhtin. All rights reserved.
//

import UIKit

extension UIView {
    
    public var screenshot: UIImage?  {
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, 1.0);
        if let _ = UIGraphicsGetCurrentContext() {
            drawHierarchy(in: bounds, afterScreenUpdates: true)
            let screenshot = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return screenshot
        }
        return nil
    }
}
