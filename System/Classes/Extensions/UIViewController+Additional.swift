//
//  UIViewController+Additional.swift
//  System
//
//  Created by Артём Шляхтин on 09/03/2017.
//  Copyright © 2017 Artem Shlyakhtin. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

extension UIViewController {
    
    public var topViewController: UIViewController? {
        get {
            var topController = UIApplication.shared.windows.first?.rootViewController
            while topController?.presentedViewController != nil {
                topController = topController?.presentedViewController
            }
            return topController
        }
    }
    
    /**
     Функция запрашивает разрешение на доступ к камере
     */
    public func requestCameraPermission(completionHandler handler: @escaping (Bool) -> ()) {
        let status = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
        switch status {
        case .authorized:
            handler(true)
        case .notDetermined:
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo) { granted in
                handler(granted)
            }
        default:
            handler(false)
            showAlertForChangeAppSettingsWithTitle("Приложению необходим доступ к Камере")
        }
    }
    
    /**
     Функция запрашивает разрешение на доступ к фото
     */
    public func requestPhotoLibraryPermission(completionHandler handler: @escaping (Bool) -> ()) {
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            handler(true)
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization { (granted) in
                if granted == .authorized { handler(true) }
            }
        default:
            handler(false)
            showAlertForChangeAppSettingsWithTitle("Приложению необходим доступ к Фото")
        }
    }
    
    /**
     Функция показывает опповещение о необходимости перейти в настройки приложения.
     */
    fileprivate func showAlertForChangeAppSettingsWithTitle(_ title: String) {
        let alert = UIAlertController(title: title, message: "Хотите ли вы изменить настройки?", preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default) { (action) in
            guard let url = URL(string: UIApplicationOpenSettingsURLString) else { return }
            UIApplication.shared.openURL(url)
        }
        let cancel = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        alert.addAction(ok)
        alert.addAction(cancel)
        OperationQueue.main.addOperation {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
}
