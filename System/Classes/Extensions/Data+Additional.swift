//
//  Data+Additional.swift
//  System
//
//  Created by Артём Шляхтин on 21/03/2017.
//  Copyright © 2017 Artem Shlyakhtin. All rights reserved.
//

import UIKit

// MARK: - JSON

extension Data {
    
    public var jsonValue: Any? {
        get {
            let json = try? JSONSerialization.jsonObject(with: self, options: .mutableContainers)
            return json
        }
    }
    
}
