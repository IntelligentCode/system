//
//  UIImage+Additional.swift
//  System
//
//  Created by Артём Шляхтин on 11/03/2017.
//  Copyright © 2017 Artem Shlyakhtin. All rights reserved.
//

import UIKit

extension UIImage {
    
    public class func imageWithColor(color: UIColor) -> UIImage? {
        let size = CGSize(width: 1.0, height: 1.0)
        let rect = CGRect(origin: CGPoint.zero, size: size)
        
        UIGraphicsBeginImageContext(size)
        let context = UIGraphicsGetCurrentContext()
        context?.setFillColor(color.cgColor)
        context?.fill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
    
}

// MARK: - Resize

extension UIImage {
    
    public func resize(toHeight height: CGFloat) -> UIImage? {
        let scale = height/self.size.height
        return resize(toScale: scale)
    }

    public func resize(toWidth width: CGFloat) -> UIImage? {
        let scale = width/self.size.width
        return resize(toScale: scale)
    }
    
    public func resize(toScale scale: CGFloat) -> UIImage? {
        let newHeight = self.size.height * scale
        let newWidth = self.size.width * scale
        let size = CGSize(width: newWidth, height: newHeight)
        return resize(size: size)
    }
    
    public func resize(size: CGSize) -> UIImage? {
        UIGraphicsBeginImageContext(size)
        self.draw(in: CGRect(origin: CGPoint.zero, size: size))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        return newImage
    }
    
}
