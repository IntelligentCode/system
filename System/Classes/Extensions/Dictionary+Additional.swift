//
//  Dictionary+Additional.swift
//  System
//
//  Created by Артём Шляхтин on 21/03/2017.
//  Copyright © 2017 Artem Shlyakhtin. All rights reserved.
//

import Foundation

// MARK: - JSON

extension Dictionary {
    
    public var jsonData: Data? {
        get {
            if JSONSerialization.isValidJSONObject(self) == false { return nil }
            let data = try? JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            return data
        }
    }
    
}
