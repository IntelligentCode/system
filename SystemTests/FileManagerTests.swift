//
//  FileManagerTests.swift
//  System
//
//  Created by Artem Salimyanov on 02.02.17.
//  Copyright © 2017 Артём Шляхтин. All rights reserved.
//

import XCTest
@testable import System

class FileManagerTests: XCTestCase {
    
    func testGettingCorrectDocumentDirectory() {
        let documentDirectory = FileManager.default.applicationDocumentsDirectory()?.absoluteString
        print("document directory: \(documentDirectory)")
        if let documentDirectory = documentDirectory {
            let isContainPathUrl = documentDirectory.contains("data/Documents/")
            XCTAssert(isContainPathUrl)
        } else {
            XCTAssert(false, "Directory not determine")
        }
    }
}
