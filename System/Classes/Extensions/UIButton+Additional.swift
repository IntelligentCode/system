//
//  UIButton+BorderColor.swift
//  System
//
//  Created by Артем Шляхтин on 28.08.15.
//  Copyright (c) 2015 Артем Шляхтин. All rights reserved.
//

import UIKit

extension UIButton {
    
    /**
     Свойство позволяет настроить цвет границы.
    */
    public var borderColor : UIColor? {
        set (color) {
          self.layer.borderColor = color?.cgColor
        }
        
        get {
            let color = UIColor(cgColor: self.layer.borderColor!)
            return color
        }
    }
    
}
